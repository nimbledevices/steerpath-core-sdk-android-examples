package com.steerpath.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;

import com.steerpath.demo.utils.SetupHelper;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;
import com.steerpath.sdk.meta.MetaQueryResult;

import java.util.ArrayList;

/*
* We use splash screen for configuring client and loading buildings before entering SteerpathMapView
*/
public class SplashActivity extends AppCompatActivity implements MetaLoader.LoadListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This piece of code allows user to enter back to previous state in MainActivity
        // when returning to app by pressing launcher icon.
        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }

        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onNewIntent (Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(sdkReadyBroadcastReceiver, new IntentFilter(DemoApplication.BROADCAST_SDK_READY));
        SetupHelper.initApikey(this, getIntent());
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sdkReadyBroadcastReceiver);
        Log.d("SplashActivity", "onStop");
        finish();
    }

    public void loadBuildings(Context context) {
        MetaQuery.Builder query = new MetaQuery.Builder(context, MetaQuery.DataType.BUILDINGS);
        MetaLoader.load(query.build(), this);
    }

    /*
    * Callback informs when buildings are loaded. Start activity with buildings as intent extra.
    */
    @Override
    public void onLoaded(MetaQueryResult result) {
        ArrayList<MetaFeature> buildings = result.getMetaFeatures();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putParcelableArrayListExtra("buildings", buildings);
        Log.d("SplashActivity", "onBuildingsLoaded");
        startActivity(intent);
    }

    /*
    * Application class notifies when client is ready. After that lets load all the buildings
    */
    private BroadcastReceiver sdkReadyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            loadBuildings(context);
        }
    };
}
