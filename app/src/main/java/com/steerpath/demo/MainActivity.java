package com.steerpath.demo;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.steerpath.demo.advanced.AboutActivity;
import com.steerpath.demo.advanced.LiveLoginActivity;
import com.steerpath.demo.advanced.SettingsActivity;
import com.steerpath.demo.advanced.SetupActivity;
import com.steerpath.demo.examples.MarkersActivity;
import com.steerpath.demo.examples.FakeLocationActivity;
import com.steerpath.demo.examples.GeofenceActivity;
import com.steerpath.demo.examples.MetaLoaderActivity;
import com.steerpath.demo.examples.PositioningActivity;
import com.steerpath.demo.examples.SteerpathMapViewActivity;
import com.steerpath.demo.examples.SymbolLayerPOIActivity;
import com.steerpath.demo.examples.WaypointsActivity;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.demo.utils.SettingsHelper;
import com.steerpath.sdk.assettracking.AssetGateway;
import com.steerpath.sdk.live.LiveMapOptions;
import com.steerpath.sdk.location.Location;
import com.steerpath.sdk.location.LocationRequest;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.InfoBottomSheetOptions;
import com.steerpath.sdk.maps.OnMapReadyCallback;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapOptions;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener,
        NavigationView.OnNavigationItemSelectedListener {

    public static final int CLIENT_CHANGED = 1;
    public static final int REFRESH_LOCATION_PROVIDER = 2;
    public static final int SETTINGS_CHANGED = 3;
    public static final int LIVE_STATUS_CHANGED = 4;

    private Toolbar toolbar;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private MenuItem checkedItem;
    private ImageView apikeyInfo;

    private ArrayList<MetaFeature> buildings;
    private MetaFeature activeBuilding;

    private List<MenuItem> buildingItems = new ArrayList<>();
    private MenuItem selectedBuildingItem;

    private SteerpathMapView mapView = null;
    private SteerpathMap map = null;
    private SteerpathMapFragment steerpathMapFragment;

    private Intent newApikeyIntent;

    private boolean isClientChanged = false;
    private boolean isBluetoothEnabled = false;
    private int selectedNavItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setIcon(R.drawable.steerpath_logo_white);
        }

        steerpathMapFragment = SteerpathMapFragment.newInstance();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, steerpathMapFragment, "steerpath-map-fragment").commit();
        }

        navigationView = findViewById(R.id.nav_view);
        drawerLayout = findViewById(R.id.drawer_layout);

        drawerLayout.addDrawerListener(initDrawerListener());
        navigationView.setNavigationItemSelectedListener(this);

        buildings = getIntent().getParcelableArrayListExtra("buildings");
        addBuildingsToNavDrawer();

        if (SettingsHelper.useAssetGateway()) {
            AssetGateway.startAssetGatewayService(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CLIENT_CHANGED) {
            if (resultCode == RESULT_OK) {
                //We need to init new map fragment after client has been changed
                newApikeyIntent = intent;
                replaceMapFragment(DemoApplication.liveEnabled);
                isClientChanged = true;
            }
        }

        /*
         * Refresh location sources after been using fake location source.
         * Fake location source automatically disables bluetooth so here we
         * turn that back on.
         */
        if (requestCode == REFRESH_LOCATION_PROVIDER) {
            if (resultCode == RESULT_OK) {
                if (isBluetoothEnabled && !BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    BluetoothAdapter.getDefaultAdapter().enable();
                }
                setPositioningAccuracy();
            }
        }

        /*
         * Refresh map because monitor has been enabled/disabled
         */
        if (requestCode == SETTINGS_CHANGED) {
            if (resultCode == RESULT_OK) {
                newApikeyIntent = intent;
                replaceMapFragment(DemoApplication.liveEnabled);
            }
        }

        if (requestCode == LIVE_STATUS_CHANGED) {
            if (resultCode == RESULT_OK) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals("LOGGED_IN") || intent.getAction().equals("LOGGED_OUT")) {
                        replaceMapFragment(DemoApplication.liveEnabled);
                        return;
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (DemoApplication.USES_DEFAULT_CONFIG) {
            apikeyInfo.setVisibility(VISIBLE);
        } else {
            apikeyInfo.setVisibility(GONE);
        }

        if (isClientChanged) {
            if (newApikeyIntent.getBooleanExtra("restart_gateway", false)) {
                AssetGateway.startAssetGatewayService(this);
            }
            buildings = newApikeyIntent.getParcelableArrayListExtra("newBuildings");
            addBuildingsToNavDrawer();
            isClientChanged = false;
        }
    }

    @Override
    public void onBackPressed() {
        if (!mapView.onBackPressed()) {
            moveTaskToBack(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isBluetoothEnabled = BluetoothAdapter.getDefaultAdapter().isEnabled();
    }

    @Override
    public void onDestroy() {
        if (SettingsHelper.useAssetGateway()) {
            AssetGateway.stopAssetGatewayService(this);
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onMapViewReady(SteerpathMapView steerpathMapView) {
        this.mapView = steerpathMapView;
        this.mapView.getMapAsync(mapReadyCallback);
    }

    private OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(SteerpathMap steerpathMap) {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MainActivity.this.setPositioningAccuracy();

            if (!buildings.isEmpty()) {
                MainActivity.this.onNavigationItemSelected(navigationView.getMenu().getItem(0).getSubMenu().getItem(0));
            }

            MainActivity.this.setOnMapClickListener();
            MainActivity.this.setOnMapChangedListener();
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        selectedNavItem = item.getItemId();

        // With these two if clauses we can keep the active building item always checked.
        // All but building items are non-checkable.
        if (checkedItem != null && item.isCheckable()) {
            checkedItem.setChecked(false);
        }

        if (item.isCheckable()) {
            item.setChecked(true);
            checkedItem = item;
        }

        checkIfBuilding(item);
        drawerLayout.closeDrawers();

        return false;
    }

    //Open selected Activity/Fragment after drawer is closed to avoid drawer lagging.
    private ActionBarDrawerToggle initDrawerListener() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                syncState();

                if (selectedNavItem != -1) {
                    switch (selectedNavItem) {
                        case R.id.menu_about:
                            Intent aboutIntent = new Intent(MainActivity.this, AboutActivity.class);
                            startActivity(aboutIntent);
                            break;
                        case R.id.menu_apikey:
                            Intent apikeyIntent = new Intent(MainActivity.this, SetupActivity.class);
                            startActivityForResult(apikeyIntent, CLIENT_CHANGED);
                            break;
                        case R.id.menu_settings:
                            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                            settingsIntent.putExtra("building", activeBuilding);
                            startActivityForResult(settingsIntent, SETTINGS_CHANGED);
                            break;
                        case R.id.steerpath_live:
                            Intent liveLoginIntent = new Intent(MainActivity.this, LiveLoginActivity.class);
                            startActivityForResult(liveLoginIntent, LIVE_STATUS_CHANGED);
                            break;

                         // EXAMPLES
                        case R.id.simple_annotation_activity:
                            Intent simpleAnnotationsIntent = new Intent(MainActivity.this, MarkersActivity.class);
                            simpleAnnotationsIntent.putExtra("building", activeBuilding);
                            startActivity(simpleAnnotationsIntent);
                            break;
                        case R.id.fake_location_activity:
                            Intent fakeLocationIntent = new Intent(MainActivity.this, FakeLocationActivity.class);
                            fakeLocationIntent.putExtra("building", activeBuilding);
                            startActivityForResult(fakeLocationIntent, REFRESH_LOCATION_PROVIDER);
                            break;
                        case R.id.geofence_activity:
                            Intent geofenceIntent = new Intent(MainActivity.this, GeofenceActivity.class);
                            geofenceIntent.putExtra("building", activeBuilding);
                            startActivity(geofenceIntent);
                            break;
                        case R.id.meta_loader_activity:
                            Intent metaLoaderIntent = new Intent(MainActivity.this, MetaLoaderActivity.class);
                            metaLoaderIntent.putExtra("building", activeBuilding);
                            startActivity(metaLoaderIntent);
                            break;
                        case R.id.positioning_activity:
                            Intent positioningIntent = new Intent(MainActivity.this, PositioningActivity.class);
                            startActivity(positioningIntent);
                            break;
                        case R.id.symbol_layer_activity:
                            Intent symbolLayerIntent = new Intent(MainActivity.this, SymbolLayerPOIActivity.class);
                            symbolLayerIntent.putExtra("building", activeBuilding);
                            startActivity(symbolLayerIntent);
                            break;
                        case R.id.waypoints_activity:
                            Intent waypointsIntent = new Intent(MainActivity.this, WaypointsActivity.class);
                            waypointsIntent.putExtra("building", activeBuilding);
                            startActivity(waypointsIntent);
                            break;
                        case R.id.legacy_mapview:
                            Intent legacyMapViewIntent = new Intent(MainActivity.this, SteerpathMapViewActivity.class);
                            legacyMapViewIntent.putExtra("building", activeBuilding);
                            startActivity(legacyMapViewIntent);
                            break;
                    }
                }

                selectedNavItem = -1;
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        actionBarDrawerToggle.syncState();
        return actionBarDrawerToggle;
    }

    /*
     * Add all buildings under certain API Key to navigation view's menu
     */
    private void addBuildingsToNavDrawer() {

        final Menu menu = navigationView.getMenu();
        final SubMenu subMenu = menu.getItem(0).getSubMenu();
        apikeyInfo = menu.findItem(R.id.menu_apikey).getActionView().findViewById(R.id.icon_apikey_info);
        apikeyInfo.setVisibility(GONE);

        if (!buildings.isEmpty()) {
            subMenu.clear();
            int orderCount = 0;
            for (MetaFeature building : buildings) {
                subMenu.add(0, building.getId().hashCode(), orderCount, building.getTitle());
                orderCount++;
            }

            int size = subMenu.size();
            for (int i = 0; i < size; i++) {
                MenuItem item = subMenu.getItem(i);
                item.setIcon(R.drawable.ic_building);
                item.setCheckable(true);
                item.isCheckable();
                buildingItems.add(item);
            }
        }
    }

    /*
     * This checks if the navigation item clicked is a building. Moves focus to selected building.
     */
    private void checkIfBuilding(MenuItem item) {
        for (MetaFeature building : buildings) {
            if (item.getItemId() == building.getId().hashCode()) {
                if (activeBuilding == null || activeBuilding != building) {
                    activeBuilding = building;
                    selectedBuildingItem = item;
                }

                focusMap();
            }
        }
    }

    private void setActiveBuildingMenuItem() {
        for (MenuItem item : buildingItems) {
            if (item.getItemId() == activeBuilding.getId().hashCode()) {
                selectedBuildingItem.setChecked(false);
                selectedBuildingItem = item;
                selectedBuildingItem.setChecked(true);
            }
        }
    }

    private void setOnMapClickListener() {
        map.addOnMapClickListener(point -> {
            MetaFeature feature = map.getRenderedMetaFeature(point);
            if (feature != null) {
                mapView.showInfoBottomSheet(InfoBottomSheetOptions.from(feature));
            }

            return true;
        });
    }

    /*
     * Focus map to a selected building or user location.
     */
    private void focusMap() {
        Location userLocation = LocationServices.getFusedLocationProvider().getUserLocation();

        if (map != null) {
            if (userLocation != null && userLocation.getBuildingId().equals(activeBuilding.getId())) {
                MapHelper.moveCameraTo(map, new LatLng(userLocation.getLatitude(), userLocation.getLongitude()));
            } else {
                MapHelper.moveCameraTo(map, activeBuilding);
            }
        } else {
            mapView.getMapAsync(mapReadyCallback);
        }
    }

    private void setOnMapChangedListener() {
        map.addOnCameraMoveListener(() -> {
            if (map.getFocusedBuilding() != null) {
                if (!map.getFocusedBuilding().getId().equals(activeBuilding.getId())) {
                    for (MetaFeature building : buildings) {
                        if (building.getId().equals(map.getFocusedBuilding().getId())) {
                            activeBuilding = building;
                            setActiveBuildingMenuItem();
                        }
                    }
                }
            }
        });
    }

    /*
     * Replace already existing map fragment with new one. Used after changing client
     */
    private void replaceMapFragment(boolean liveEnabled) {

        SteerpathMapFragment newFragment;

        if (liveEnabled) {
            SteerpathMapOptions mapOptions = new SteerpathMapOptions()
                    .liveOptions(new LiveMapOptions().showThisDevice(DemoApplication.showThisDeviceLive));
            newFragment = SteerpathMapFragment.newInstance(mapOptions);
        } else {
            newFragment = SteerpathMapFragment.newInstance();
        }

        getSupportFragmentManager().beginTransaction().replace(
                steerpathMapFragment.getId(), newFragment, "steerpath-map-fragment").commitAllowingStateLoss();
        steerpathMapFragment = newFragment;
    }

    // If high accuracy positioning is enabled, set new location request.
    private void setPositioningAccuracy() {
        LocationServices.getFusedLocationProvider().refreshLocationSources();
        if (SettingsHelper.useHighAccuracyPositioning()) {
            map.setLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
        } else {
            map.setLocationRequest(LocationRequest.create());
        }
    }
}
