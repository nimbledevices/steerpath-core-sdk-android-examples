package com.steerpath.demo.examples;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;
import com.steerpath.sdk.meta.MetaQueryResult;

import java.util.ArrayList;

public class MetaLoaderActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener, MetaLoader.LoadListener, MetaLoaderDialogFragment.OnItemSelectedListener {

    private SteerpathMap map = null;

    private MetaFeature building;
    private ArrayList<MetaFeature> features;

    private boolean showDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meta_loader);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.meta_loader);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        loadPois();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.meta_loader_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.meta_loader_fragment) {
            if (!showDialog) {
                Toast.makeText(this, "Loading MetaFeatures, please wait..", Toast.LENGTH_SHORT).show();
            } else {
                MetaLoaderDialogFragment dialogFragment = MetaLoaderDialogFragment.newInstance(building, features);
                dialogFragment.show(getSupportFragmentManager(), "dialog");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadPois() {
        MetaLoader.load(new MetaQuery.Builder(this, MetaQuery.DataType.POINTS_OF_INTEREST)
                .buildingRef(building.getBuildingRef())
                .build(), this);
    }

    @Override
    public void onLoaded(MetaQueryResult metaQueryResult) {
        features = metaQueryResult.getMetaFeatures();
        showDialog = true;
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);
        });
    }

    /*
     * Add annotation and move camera to selected POI
     */
    @Override
    public void onSelected(MetaFeature feature) {
        FeatureCollectionFactory.removeMarkers(map);

        FeatureCollectionFactory.addMarker(map, feature);

        MapHelper.moveCameraTo(map, feature);
        map.setActiveLevel(building.getId(), feature.getFloorIndex());
    }
}
