package com.steerpath.demo.examples;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.sdk.directions.DirectionsException;
import com.steerpath.sdk.directions.DirectionsResponse;
import com.steerpath.sdk.directions.RouteListener;
import com.steerpath.sdk.directions.RoutePlan;
import com.steerpath.sdk.directions.RouteStep;
import com.steerpath.sdk.directions.RouteTrackerProgress;
import com.steerpath.sdk.directions.Waypoint;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class WaypointsActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener{

    private ProgressBar progressBar = null;

    private SteerpathMapView mapView = null;
    private SteerpathMap map = null;

    private MetaFeature building;

    private List<MetaFeature> markers = new ArrayList<>();
    private List<Waypoint> waypoints = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.waypoints));
        }

        progressBar = findViewById(R.id.steerpath_map_progressbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        LocationServices.getFusedLocationProvider().refreshLocationSources();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.waypoints_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_all_annotations:
                if (mapView.isNavigating()) {
                    mapView.stopNavigation();
                } else {
                    clearAllAnnotations();
                }

                return true;

            case R.id.get_directions:
                if (!waypoints.isEmpty()) {
                    showRoutePreview();
                } else {
                    Toast.makeText(this, "Select few waypoints  first", Toast.LENGTH_SHORT).show();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {
        this.mapView = mapView;
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);
            setOnMapClickListenerForMultiWaypointNavigation();
        });
    }

    private void setOnMapClickListenerForMultiWaypointNavigation() {
        map.addOnMapClickListener(point -> {
            MetaFeature feature = map.getRenderedMetaFeature(point);
            if (feature != null) {
                markers.add(feature);
                FeatureCollectionFactory.addMarkers(map, markers);
                waypoints.add(new Waypoint(feature));
            }
            return true;
        });
    }

    private void clearAllAnnotations() {
        markers.clear();
        FeatureCollectionFactory.removeMarkers(map);
        waypoints.clear();
    }

    private void showRoutePreview() {
        progressBar.setVisibility(VISIBLE);
        RoutePlan plan = new RoutePlan.Builder()
                .waypoints(waypoints)

                // destination can be set explicitly. If not set, last Waypoint will be treated as destination.
                //.destination(destination)

                // change how much map will be zoomed out on preview mode
                // depending on number of waypoints, you may want to zoom preview little bit more or less
                .previewCameraPadding(350)

                .build();

        mapView.previewRoute(plan, new RouteListener() {

            @Override
            public void onDirections(DirectionsResponse directions) {
                progressBar.setVisibility(GONE);
            }

            @Override
            public void onProgress(RouteTrackerProgress progress) {

            }

            @Override
            public void onStepEntered(RouteStep step) {

            }

            @Override
            public void onWaypointReached(Waypoint waypoint) {
                // OPTIONS:
                // 1. You may show WaypointViewHolder when Waypoint has been reached and let user
                // continue navigation manually:
                //mapView.showWaypointViewHolder(waypoint);

                // 2. You may continue to next Waypoint automatically:
                int index = plan.waypoints.indexOf(waypoint);
                if (index < plan.waypoints.size()-1) {
                    index++;
                    Waypoint next = plan.waypoints.get(index);
                    mapView.navigateTo(next);
                }
            }

            @Override
            public void onDestinationReached() {
                // OPTIONAL:
                //mapView.stopNavigation();
            }

            @Override
            public void onNavigationStopped() {
                clearAllAnnotations();
            }

            @Override
            public void onError(DirectionsException error) {
                progressBar.setVisibility(GONE);
                Toast.makeText(WaypointsActivity.this, "Routing failed", Toast.LENGTH_SHORT).show();
            }
        });

        // NOTE: SDK will switch automatically to NORMAL mode. This is to prevent camera wandering around.
        // FOLLOW_USER mode may interrupt preview route camera positioning.
        //map.setMapMode(SteerpathMap.MapMode.FOLLOW_USER);
    }


    @Override
    public void onStop() {
        super.onStop();
        // if navigation is not stopped, positioning thread stays alive when app is backgrounded
        if (mapView != null) {
            mapView.stopNavigation();
        }
    }

}
