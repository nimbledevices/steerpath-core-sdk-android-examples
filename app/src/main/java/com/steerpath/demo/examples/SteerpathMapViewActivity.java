package com.steerpath.demo.examples;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.WindowManager;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.demo.utils.PermissionUtils;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;

/**
 * Well, term "legacy" here is bit exaggerative. SteerpathMapView is valid way to implement map, but doing so
 * requires more work for you.
 *
 * SteerpathMapFragment is a wrapper for SteerpathMapView and implements following:
 * - Mapbox lifecycle methods
 * - sets default map style url for you
 * - implements permission dialog (LocateMe -button)
 */

public class SteerpathMapViewActivity extends AppCompatActivity {

    private SteerpathMapView mapView;

    private MetaFeature building;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.legacy_mapview);
        }

        building = getIntent().getParcelableExtra("building");
        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        mapView = findViewById(R.id.mapview);
        // mandatory call. Mapbox MapView initializer.
        mapView.onCreate(savedInstanceState);

        /*
         * When using SteerpathMapView you have to take care of asking runtime permissions.
         * this listener gets triggered when pressing locate me button on bottom right corner.
         */
        mapView.setOnPermissionsNotGrantedListener(strings -> {
            //Request permissions here..
            PermissionUtils.requestPermissions(SteerpathMapViewActivity.this, strings, 0);
        });

        mapView.getMapAsync(steerpathMap -> {
            steerpathMap.setMyLocationEnabled(true);
            MapHelper.moveCameraTo(steerpathMap, building);
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    // Add the Mapbox lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}