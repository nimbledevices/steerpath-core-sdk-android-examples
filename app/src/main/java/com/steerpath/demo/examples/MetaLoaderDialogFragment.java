package com.steerpath.demo.examples;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.steerpath.demo.R;
import com.steerpath.demo.widgets.FeatureAdapter;
import com.steerpath.sdk.meta.MetaFeature;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MetaLoaderDialogFragment extends DialogFragment {

    private MetaFeature building;
    private List<MetaFeature> features;

    static MetaLoaderDialogFragment newInstance(MetaFeature building, ArrayList<MetaFeature> features) {
        MetaLoaderDialogFragment fragment = new MetaLoaderDialogFragment();

        Bundle args = new Bundle();
        args.putParcelable("building", building);
        args.putParcelableArrayList("features", features);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedIsntanceState) {
        super.onCreate(savedIsntanceState);
        setStyle(STYLE_NO_FRAME, R.style.Steepath_Theme_Dialog);

        if (getArguments() != null) {
            building = getArguments().getParcelable("building");
            features = getArguments().getParcelableArrayList("features");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        assert window != null;
        WindowManager.LayoutParams params = window.getAttributes();
        window.setGravity(Gravity.TOP);
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(params);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedIstanceState) {
        View dialogView = inflater.inflate(R.layout.dialog_fragment_meta_loader, container, false);

        Toolbar toolbar = dialogView.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.meta_loader);
        toolbar.setSubtitle("Building: " + building.getId() + ", Loaded: " + features.size() + " POIs");
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(view -> getDialog().dismiss());

        RecyclerView recyclerView = dialogView.findViewById(R.id.features_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext()), DividerItemDecoration.VERTICAL));
        recyclerView.setBackgroundColor(getResources().getColor(R.color.white));

        FeatureAdapter adapter = new FeatureAdapter(getContext(), features);

        recyclerView.setAdapter(adapter);

        //Select POI from a list and show it on mapView of MetaLoaderActivity
        adapter.setOnItemClickListener((view, position) -> {
            OnItemSelectedListener activity = (OnItemSelectedListener) getActivity();
            if (activity != null) {
                activity.onSelected(features.get(position));
            }

            getDialog().dismiss();
        });

        return dialogView;
    }

    public interface OnItemSelectedListener {
        void onSelected(MetaFeature feature);
    }
}
