package com.steerpath.demo.examples;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.WindowManager;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;
import com.steerpath.sdk.meta.MetaQueryResult;

import java.util.ArrayList;

import static com.steerpath.sdk.meta.MetaQuery.DataType.POINTS_OF_INTEREST;

public class MarkersActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener, MetaLoader.LoadListener {

    private SteerpathMap map = null;

    private MetaFeature building;

    private ArrayList<MetaFeature> poiList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.simple_markers));
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        LocationServices.getFusedLocationProvider().refreshLocationSources();
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);

            loadPOIs();
        });
    }

    private void loadPOIs() {
        MetaLoader.load(new MetaQuery.Builder(this, POINTS_OF_INTEREST)
                .buildingRef(building.getBuildingRef())
                .build(), this);
    }

    /*
     * Add marker to MetaFeatures with specified tag
     */
    @Override
    public void onLoaded(MetaQueryResult result) {
        if (!result.hasError()) {
            for (MetaFeature feature : result.getMetaFeatures()) {
                if (feature.getTags().contains("room")) {
                    poiList.add(feature);
                }
            }
            FeatureCollectionFactory.addMarkers(map, poiList);
            getSupportActionBar().setSubtitle(building.getTitle() + " (id=" + building.getId() + ") has " + result.getMetaFeatures().size() + " POIs");
        }
    }
}
