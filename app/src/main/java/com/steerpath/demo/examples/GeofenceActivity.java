package com.steerpath.demo.examples;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.sdk.geofence.Geofence;
import com.steerpath.sdk.geofence.GeofenceListener;
import com.steerpath.sdk.geofence.GeofencingApi;
import com.steerpath.sdk.geofence.GeofencingEvent;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;
import com.steerpath.sdk.utils.GeoJsonHelper;
import com.steerpath.sdk.utils.internal.Monitor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.steerpath.sdk.meta.MetaQuery.DataType.POINTS_OF_INTEREST;

public class GeofenceActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener, GeofenceListener {

    private SteerpathMap map = null;
    private SteerpathMapView mapView = null;

    private MetaFeature building;

    private List<String> geofenceRequestIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.geofences));
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        LocationServices.getFusedLocationProvider().refreshLocationSources();

        GeofencingApi.getApi(this).setLocationProvider(LocationServices.getFusedLocationProvider());
    }

    @Override
    public void onStart() {
        super.onStart();
        GeofencingApi.getApi(this).addGeofenceListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        GeofencingApi.getApi(this).removeGeofenceListener(this);
        GeofencingApi.getApi(this).removeGeofences(geofenceRequestIds);
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {

        this.mapView = mapView;
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);

            loadGeofenceArea();
        });
    }

    /*
     * Load buildings and add geofences
     */
    private void loadGeofenceArea() {
        MetaLoader.load(
                new MetaQuery.Builder(this, POINTS_OF_INTEREST)
                        .build()

                , result -> {
                    if (!result.hasError()) {
                        addGeofences(result.getMetaFeatures(), result.getJson());
                    } else {
                        Monitor.add(Monitor.TAG_ERROR, "failed to register geofences: " + result.getErrorMessage());
                    }
                });
    }

    // For demonstration, let's add the geofence only for the first MetaFeature that meets the criteria.
    // The geofence will be highlighted with color.
    private void addGeofences(List<MetaFeature> features, JSONObject featureCollection) {
        for (MetaFeature feature : features) {
            JSONObject polygon = GeoJsonHelper.getPolygonJson(feature, featureCollection);
            // Let's add the geofences only for one floor
            if (polygon != null) {
                FeatureCollectionFactory.addGeofence(this, map, feature.getId() + "-source", feature.getId() + "-layer", polygon);
                addGeofence(polygon, feature.getFloorIndex(), feature.getId(), feature.getTitle());
                break;
            }
        }
    }

    private void addGeofence(JSONObject geometry, int floorIndex, String id, String info) {
        GeofencingApi api = GeofencingApi.getApi(this);
        try {
            api.addGeofence(new Geofence.Builder()
                    .addPolygon(geometry)
                    .setInfo(info)
                    .setLevelIndex(floorIndex)
                    .setRequestId(id)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setLoiteringDelay(5000)
                    .build());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onGeofencingEvent(GeofencingEvent geofencingEvent) {
        List<Geofence> geos = geofencingEvent.getTriggeringGeofences();
        if (!geofencingEvent.hasError() && geos != null && geos.size() > 0) {
            switch (geofencingEvent.getGeofenceTransition()) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    Toast.makeText(mapView.getContext(), "ENTER " + geofencingEvent.getTriggeringGeofences().get(0).getInfo(), Toast.LENGTH_LONG).show();
                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    Toast.makeText(mapView.getContext(), "DWELL " + geofencingEvent.getTriggeringGeofences().get(0).getInfo(), Toast.LENGTH_LONG).show();
                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    Toast.makeText(mapView.getContext(), "EXIT " + geofencingEvent.getTriggeringGeofences().get(0).getInfo(), Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}
