package com.steerpath.demo.examples;

import android.graphics.PointF;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mapbox.geojson.Feature;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.demo.utils.MapHelper;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaFeatureParser;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;
import com.steerpath.sdk.utils.GeoJsonHelper;
import com.steerpath.sdk.utils.internal.DrawableUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.steerpath.sdk.meta.MetaQuery.DataType.POINTS_OF_INTEREST;

public class SymbolLayerPOIActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener, View.OnClickListener {

    private SteerpathMap map= null;
    private SteerpathMapView mapView = null;

    private MetaFeature building;

    private ArrayList<MetaFeature> allPois;
    private MetaFeature selectedPOI;

    private String redMarker = "marker-red";
    private String blueMarker = "marker-blue";
    private String greenMarker = "marker-green";
    private String focusedMarker = "marker-focused";

    private SymbolLayer blueSymbolLayer;
    private SymbolLayer greenSymbolLayer;
    private SymbolLayer focusedSymbolLayer;

    private List<MetaFeature> allGreenPois = new ArrayList<>();
    private List<MetaFeature> allBluePois = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.symbol_layer_poi);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        LocationServices.getFusedLocationProvider().refreshLocationSources();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.symbol_layer_poi_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.show_soures_and_layers) {
            showSourcesAndLayers();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {
        this.mapView = mapView;
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);

            addImages();
            loadPois();

            setOnMapClickListener();
        });
    }

    private void setOnMapClickListener() {
        map.addOnMapClickListener(point -> {
            if (map.getRenderedMetaFeature(point) != null) {
                final PointF pixel = map.toScreenLocation(point);
                for (Feature feature : map.queryRenderedFeatures(pixel, "layer-red-marker", "layer-green-marker", "layer-blue-marker")) {
                    String id = feature.getStringProperty("id");
                    MetaFeature poi = findPOIById(id);
                    selectedPOI = poi;
                    //Just to make sure there isn't null pointer
                    if (poi != null) {
                        focusedSymbolLayer.withFilter(Expression.eq(Expression.get("id"), poi.getId()));
                        mapView.setBottomSheet(createBottomSheet(poi), null);
                    }
                }
            } else {
                focusedSymbolLayer.withFilter(Expression.eq(Expression.get("id"), "invalid"));
                mapView.setBottomSheetVisible(false);
            }
            return true;
        });
    }

    private void loadPois() {
        MetaQuery.Builder query = new MetaQuery.Builder(this, POINTS_OF_INTEREST);
        query.buildingRef(building.getBuildingRef());
        query.parser(new POIParser());
        MetaLoader.load(query.build(), result -> {
            allPois = result.getMetaFeatures();
            createSourcesAndLayers(allPois);
        });
    }

    //- - - - - - - - - - - - - - - - - - Symbol Layers - - - - - - - - - - - - - - - - - - - - - - /

    private void addImages() {
        map.addImage(greenMarker, DrawableUtils.toBitmap(getResources(), R.drawable.ic_location_green));

        map.addImage(redMarker, DrawableUtils.toBitmap(getResources(), R.drawable.ic_location_red));

        map.addImage(blueMarker, DrawableUtils.toBitmap(getResources(), R.drawable.ic_location_blue));

        map.addImage(focusedMarker, DrawableUtils.toBitmap(getResources(), R.drawable.ic_crop_free));
    }

    private void createSourcesAndLayers(List<MetaFeature> metaFeatures) {
        Expression showAllInitially = Expression.has("id");
        Expression hideAllInitially = Expression.eq(Expression.get("id"), "invalid");

        //TODO --> SymbolLayer clustering
        Source source = new GeoJsonSource("poi-source", FeatureCollectionFactory.toFeatureCollection(metaFeatures), new GeoJsonOptions()
                .withCluster(true)
                .withClusterMaxZoom(14)
                .withClusterRadius(50));

        map.addSource(source);

        map.addLayer(FeatureCollectionFactory.createSymbolLayer(map, source.getId(), "layer-red-marker", redMarker, showAllInitially));

        greenSymbolLayer = FeatureCollectionFactory.createSymbolLayer(map, source.getId(), "layer-green-marker", greenMarker, hideAllInitially);
        map.addLayer(greenSymbolLayer);
        blueSymbolLayer = FeatureCollectionFactory.createSymbolLayer(map, source.getId(), "layer-blue-marker", blueMarker, hideAllInitially);
        map.addLayer(blueSymbolLayer);
        focusedSymbolLayer = FeatureCollectionFactory.createSymbolLayer(map, source.getId(), "layer-focused-marker", focusedMarker, hideAllInitially);
        map.addLayer(focusedSymbolLayer);
    }

    @Override
    public void onClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch (v.getId()) {
            case R.id.red:
                if (checked) {
                    if (allGreenPois.contains(selectedPOI)) {
                        allGreenPois.remove(selectedPOI);
                    } else allBluePois.remove(selectedPOI);
                }
                break;

            case R.id.green:
                if (checked) {
                    allBluePois.remove(selectedPOI);
                    allGreenPois.add(selectedPOI);
                }

                break;

            case R.id.blue:
                if (checked) {
                    allGreenPois.remove(selectedPOI);
                    allBluePois.add(selectedPOI);
                }

                break;
        }

        updateLayerOptions();
    }

    private void updateLayerOptions() {

        Expression[] statementsGreen = new Expression[allGreenPois.size()];
        for (int i = 0; i < allGreenPois.size(); i++) {
            MetaFeature poi = allGreenPois.get(i);
            statementsGreen[i] = Expression.eq(Expression.get("id"), poi.getId());
        }

        greenSymbolLayer.withFilter(Expression.any(statementsGreen));

        Expression[] statementsBlue = new Expression[allBluePois.size()];
        for (int i = 0; i < allBluePois.size(); i++) {
            MetaFeature poi = allBluePois.get(i);
            statementsBlue[i] = Expression.eq(Expression.get("id"), poi.getId());
        }

        blueSymbolLayer.withFilter(Expression.any(statementsBlue));
    }

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - /

    private static class POIParser implements MetaFeatureParser {

        @Override
        public MetaFeature parseFeature(JSONObject featureJson) {
            return new MetaFeature.Builder()
                    .id(GeoJsonHelper.getId(featureJson))
                    .title(GeoJsonHelper.getTitle(featureJson))
                    .building(GeoJsonHelper.getBuildingReference(featureJson))
                    .floor(GeoJsonHelper.getFloor(featureJson, Integer.MIN_VALUE))
                    .latitude(GeoJsonHelper.getLatitude(featureJson, -1))
                    .longitude(GeoJsonHelper.getLongitude(featureJson, -1))
                    .icon(GeoJsonHelper.getIconUrl(featureJson))
                    .subtype(GeoJsonHelper.getSubType(featureJson))
                    .tags(GeoJsonHelper.getTagsList(featureJson))
                    .hasArea(GeoJsonHelper.hasArea(featureJson))
                    .localRef(GeoJsonHelper.getLocalRef(featureJson))
                    .build();
        }
    }

    private MetaFeature findPOIById(String id) {
        for (MetaFeature feature : allPois) {
            if (feature.getId().equals(id)) {
                return feature;
            }
        }

        return null;
    }

    private void showSourcesAndLayers() {
        new AlertDialog.Builder(this)
                .setTitle("Sources and Layers Info")
                .setMessage(generateSourceAndLayersInfo())
                .setPositiveButton("Close", null).show();
    }

    private String generateSourceAndLayersInfo() {
        return listSources() + "\n\n" + listLayers();
    }

    private String listSources() {
        List<Source> sources = map.getSources();
        StringBuilder builder = new StringBuilder("SOURCES:");
        for (Source source : sources) {
            builder.append("\n");
            builder.append(source.getId());
        }
        return builder.toString();
    }

    private String listLayers() {
        List<Layer> layers = map.getLayers();
        StringBuilder builder = new StringBuilder("LAYERS:");
        for (Layer layer : layers) {
            builder.append("\n");
            builder.append(layer.getId());
        }
        return builder.toString();
    }

    private View createBottomSheet(final MetaFeature metaFeature) {
        View bottomsheet = View.inflate(this, R.layout.bottomsheet_symbol_layers, null);

        TextView title = bottomsheet.findViewById(R.id.title);
        title.setText(metaFeature.getTitle());

        RadioGroup radioGroup = bottomsheet.findViewById(R.id.radio_group);
        RadioButton red = radioGroup.findViewById(R.id.red);
        red.setOnClickListener(this);
        RadioButton green = radioGroup.findViewById(R.id.green);
        green.setOnClickListener(this);
        RadioButton blue = radioGroup.findViewById(R.id.blue);
        blue.setOnClickListener(this);

        if (allGreenPois.contains(selectedPOI)) {
            green.setChecked(true);
        } else if (allBluePois.contains(selectedPOI)) {
            blue.setChecked(true);
        } else {
            red.setChecked(true);
        }

        return bottomsheet;
    }
}
