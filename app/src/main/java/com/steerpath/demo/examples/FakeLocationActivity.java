package com.steerpath.demo.examples;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.steerpath.demo.R;
import com.steerpath.demo.utils.FeatureCollectionFactory;
import com.steerpath.sdk.directions.DirectionsException;
import com.steerpath.sdk.directions.DirectionsResponse;
import com.steerpath.sdk.directions.RouteListener;
import com.steerpath.sdk.directions.RoutePlan;
import com.steerpath.sdk.location.FakeLocationProvider;
import com.steerpath.sdk.location.LocationFactory;
import com.steerpath.sdk.location.LocationServices;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.meta.MetaFeature;

import com.steerpath.demo.MainActivity;
import com.steerpath.demo.utils.MapHelper;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FakeLocationActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener{

    private ProgressBar progressBar = null;
    private SteerpathMapView mapView = null;
    private SteerpathMap map = null;
    private MetaFeature building;
    private MetaFeature selectedPOI = null;

    private FakeLocationProvider fakeLocationProvider = new FakeLocationProvider();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example_map);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.fake_location);
            getSupportActionBar().setSubtitle(R.string.fake_location_help);
        }

        progressBar = findViewById(R.id.steerpath_map_progressbar);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        building = getIntent().getParcelableExtra("building");

        LocationServices.setDefaultLocationProvider(fakeLocationProvider);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        // Setting DefaultLocationProvider back to normal
        LocationServices.setDefaultLocationProvider(LocationServices.getFusedLocationProvider());

        setResult(Activity.RESULT_OK);
        finishActivity(MainActivity.REFRESH_LOCATION_PROVIDER);
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMapViewReady(SteerpathMapView mapView) {
        this.mapView = mapView;
        mapView.getMapAsync(steerpathMap -> {
            map = steerpathMap;
            map.setMyLocationEnabled(true);
            map.setTiltGesturesEnabled(true);

            MapHelper.moveCameraTo(map, building);
            setOnMapClickListener();
        });
    }

    /*
     * This is a good practice to use fake location. We set click listener to add simple annotation marker
     * to points of interest and...
     */
    private void setOnMapClickListener() {
        map.addOnMapClickListener(point -> {
            MetaFeature feature = map.getRenderedMetaFeature(point);
            if (feature != null) {

                FeatureCollectionFactory.removeMarkers(map);

                FeatureCollectionFactory.addMarker(map, feature);
                selectedPOI = feature;

                if (fakeLocationProvider.getUserLocation() != null) {
                    showRoutePreview(selectedPOI);
                }
                else {
                    Toast.makeText(FakeLocationActivity.this, "CANNOT ROUTE WITHOUT LOCATION.", Toast.LENGTH_LONG).show();
                }
            }
            return true;
        });

        // ..long click listener to handle fake location movement
        map.addOnMapLongClickListener(point -> {
            fakeLocationProvider.setLocation(
                    LocationFactory.createFakeLocation(map, point));
            Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            assert v != null;
            v.vibrate(5);

            return true;
        });
    }

    private void showRoutePreview(MetaFeature destination) {
        progressBar.setVisibility(VISIBLE);
        doRoutePreview(new RoutePlan.Builder()
                .destination(destination)
                .rerouteThreshold(3)
                .goalDistanceThreshold(3)
                .build());
    }

    private void doRoutePreview(RoutePlan plan) {
        mapView.previewRoute(plan, new RouteListener() {

            @Override
            public void onDirections(DirectionsResponse directionsResponse) {
                progressBar.setVisibility(GONE);
            }

            @Override
            public void onNavigationStopped() {
                FeatureCollectionFactory.removeMarkers(map);
                selectedPOI = null;
            }

            @Override
            public void onError(DirectionsException e) {
                Toast.makeText(FakeLocationActivity.this, "Routing failed", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(GONE);
            }
        });
    }
}
