package com.steerpath.demo;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.github.anrwatchdog.ANRWatchDog;
import com.squareup.leakcanary.LeakCanary;
import com.steerpath.demo.database.Setup;
import com.steerpath.demo.utils.SettingsHelper;
import com.steerpath.demo.utils.SetupHelper;
import com.steerpath.sdk.common.DeveloperOptions;
import com.steerpath.sdk.common.SdkConfig;
import com.steerpath.sdk.common.SdkLoadException;
import com.steerpath.sdk.common.SdkStartCallback;
import com.steerpath.sdk.common.SteerpathClient;
import com.steerpath.sdk.live.LiveOptions;

public class DemoApplication extends Application {

    public static final String BROADCAST_SDK_READY = "steerpath.com.steerpath_android_store_app.broadcast.SDK_READY";

    public static final String PREF_NAME = "demo";
    public static final String KEY_API_KEY = "steerpath_api_key";
    public static final String NO_APIKEY = "no_apikey";

    //Default configuration for client
    public static final String DEFAULT_APIKEY = "eyJhbGciOiJSUzI1NiIsImlzcyI6InN0ZWVycGF0aC1zbWFydC1vZmZpY2UifQ.eyJjZmciOiJzdGVlcnBhdGhfY29uZmlnLmpzb24iLCJlZGl0UmlnaHRzIjoiIiwiZWlkQWNjZXNzIjoiIiwiaWF0OiI6MTU4MTQyMjI5NywianRpIjoiNGJlYmUxMDEtMTQwYS00YmMwLWE0NDctN2M0OTg2MjcxODFmIiwibWV0YUFjY2VzcyI6InkiLCJzY29wZXMiOiJ2Mi0zYWU0MTdiYS0wMzg3LTRlNzktYTUxOS03ZWU5MjE3NmRjNjgtcHVibGlzaGVkOnIiLCJzdWIiOiJ2Mi0zYWU0MTdiYS0wMzg3LTRlNzktYTUxOS03ZWU5MjE3NmRjNjgifQ.kyZ08qAmzkApM0ZQQRCD5coftySLnjE6I0zDjHSlF5Ru1HaEVVBjC25LWZJMpal8R-xE-55rhRDL62epqtN0NgRmtBMlgV3d5dIUGqtV_Al6vmTcYk8Dqc-5XeLRo-uqApgFxkBMCvsnSsCHcy_Pg3KIcllIEm72mhCPuqTE_t3YR581TBQlc3djSnxlZdA3a1EcVmSw0zAxAruv4TKomy-W-kkXTJRzoFNKBthxAVke5oW97SOm62hCJAuamRBxg6ipFOCpr6pXBoe6DHolSYt660jz36r0SZjziBdlgTpE9X3p0S320GTOw39JT54bDPZLEwH-i9KE1VrQb0qIHg";
    public static final String DEFAULT_NAME = "Steerpath Office";
    public static final String DEFAULT_REGION = "EU1";
    public static final String DEFAULT_LIVE_APIKEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZXMiOiJsaXZlOnIsdyIsImp0aSI6ImE0OGE1MTZjLTk5N2EtNDkwNS04ZGZlLTYxMGRjMmIyM2RiMyIsInN1YiI6InN0ZWVycGF0aCJ9.2GN8CMLIcmeK3_TqNmCIt_bx4QPfGn2VXNGv9wV3Fs8";

    public static boolean USES_DEFAULT_CONFIG = false;

    public static Setup currentSetup;

    public static boolean liveEnabled = false;
    private static LiveOptions liveOptions;
    public static boolean showThisDeviceLive = false;

    @Override
    public void onCreate() {
        super.onCreate();

        // NOTE: check LeakCanary.isInAnalyzerProcess() before everything else!
        // It will conveniently filter subsequent calls to onCreate().

        SettingsHelper.init(this);

        // Memory Leaks
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        // when setANRListener() is not set, app will crash on ANR
        new ANRWatchDog().setReportMainThreadOnly().start();

        // Other badness
        //enableStrictMode();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                apikeyReadyBroadcastReceiver, new IntentFilter(SetupHelper.BROADCAST_APIKEY_READY));
    }

    public static Setup getDefaultSetup() {
        return new Setup(DEFAULT_APIKEY, DEFAULT_NAME, DEFAULT_REGION, DEFAULT_LIVE_APIKEY);
    }

    /*
     * Broadcast for receiving api key, if no api key found we use default key
     */
    private BroadcastReceiver apikeyReadyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Setup setup = (Setup) intent.getSerializableExtra("setup");
            configureClient(getApplicationContext(), setup);
        }
    };

    public static void configureClient(Context context, Setup setup) {
        doConfigureClient(context, setup);
    }

    private static void doConfigureClient(Context context, Setup setup) {

        currentSetup = setup;

        int developerOptionsWithMonitor;
        if (SettingsHelper.useMonitor()) {
            developerOptionsWithMonitor = DeveloperOptions.getDefaultOptions() | DeveloperOptions.WITH_HEALTH_MONITOR;
        }
        else {
            developerOptionsWithMonitor = DeveloperOptions.DISABLED;
        }

        SdkConfig.Builder sdkBuilder = SdkConfig.fromDefault(context)
                .name(setup.getName())
                .apiKey(setup.getApikey())

                // Enables some developer options. PLEASE DISABLE DEVELOPER OPTIONS IN PRODUCTION!
                // This will add "Monitor"-button above "LocateMe"-button as a visual reminder developer options are in use
                // Use logcat filter "Monitor", for example: adb logcat *:S Monitor:V
                .developerOptions(developerOptionsWithMonitor);

        if (!setup.getRegion().equals(DEFAULT_REGION)) {
            sdkBuilder.region(setup.getRegion());
        }

        startCoreSDK(context, sdkBuilder.build());
    }

    private static void startCoreSDK(Context context, SdkConfig config) {
        SteerpathClient.getInstance().start(context, config, new SdkStartCallback() {
            @Override
            public void onStarted() {
                // Don't let user to access MapActivity before everything is ready.
                SettingsHelper.initGuideOptions(context);
                notifyReady(context);
            }

            /**
             * Called when the CoreSDK needs to be stopped before restarting.
             */
            @Override
            public void onRejected() {
                SteerpathClient.getInstance().stop();
                startCoreSDK(context, config); // just restart, it will be fine
            }

            @Override
            public void onError(SdkLoadException e) {
                e.printStackTrace();
            }
        });
    }

    private static void notifyReady(Context context) {
        Intent intent = new Intent(BROADCAST_SDK_READY);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void setLiveOptions(LiveOptions opts) {
        liveOptions = opts;
    }

    public static LiveOptions getLiveOptions() {
        return liveOptions;
    }
}