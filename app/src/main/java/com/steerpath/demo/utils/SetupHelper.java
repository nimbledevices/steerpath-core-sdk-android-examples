package com.steerpath.demo.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.UrlQuerySanitizer;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import com.steerpath.demo.DemoApplication;
import com.steerpath.demo.database.DatabaseHelper;
import com.steerpath.demo.database.Setup;
import com.steerpath.demo.database.SetupDatabase;

import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class SetupHelper {

    public static final String BROADCAST_APIKEY_READY = "steerpath.com.steerpath_android_store_app.broadcast.APIKEY_READY";

    private static final OkHttpClient DEFAULT_OK_HTTP_CLIENT;
    static {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(10, TimeUnit.SECONDS);
        builder.writeTimeout(10, TimeUnit.SECONDS);
        builder.writeTimeout(10, TimeUnit.SECONDS);
        DEFAULT_OK_HTTP_CLIENT = builder.build();
    }

    /*
     * Checks the source of APIKEY. Whether to use deeplink or shared preferences. If no apikeys, then we use default apikey.
     */
    public static void initApikey(Activity activity, Intent intent) {
        String[] deeplink = readDeepLink(intent);
        Setup defaultSetup = DemoApplication.getDefaultSetup();

        if (deeplink != null) {
            if (DemoApplication.USES_DEFAULT_CONFIG) DemoApplication.USES_DEFAULT_CONFIG = false;

            new DatabaseHelper.SetupLoaderTask(activity, setups -> {
                String apikey = deeplink[0];
                String name = deeplink[1];
                String region = deeplink[2];
                String liveApikey = deeplink[3];

                if (name != null) {
                    name = name.replace("_", " ");
                } else {
                    int unnamedCount = 1;
                    for (Setup setup : setups) {
                        if (setup.getName().contains("unnamed")) {
                            unnamedCount++;
                        }
                    }

                    name = "unnamed_" + unnamedCount;
                }

                Setup setup = new Setup(apikey, name, region);

                if (liveApikey != null) {
                    setup.setLiveApikey(liveApikey);
                }

                if (setups.size() == 0) {
                    SetupDatabase.getInstance(activity).insertSetup(defaultSetup);
                }

                SetupDatabase.getInstance(activity).getSetup(apikey, setup1 -> {
                    if (setup1 != null) {
                        setup1.setApikey(setup.getApikey());
                        setup1.setRegion(setup.getRegion());
                        setup1.setName(setup.getName());
                        if (setup.getLiveApikey() != null) {
                            setup1.setLiveApikey(setup.getLiveApikey());
                        }
                        SetupDatabase.getInstance(activity).updateSetup(setup1);
                    } else {
                        SetupDatabase.getInstance(activity).insertSetup(setup);
                    }
                });

                initSetup(activity, setup);
            }).execute();

        } else {
            SharedPreferences preferences = activity.getSharedPreferences(DemoApplication.PREF_NAME, MODE_PRIVATE);
            String existingApikey = preferences.getString(DemoApplication.KEY_API_KEY, DemoApplication.NO_APIKEY);

            if (existingApikey.equals(DemoApplication.NO_APIKEY)) {
                DemoApplication.USES_DEFAULT_CONFIG = true;
                initSetup(activity, defaultSetup);

            } else initSetup(activity, getCurrentSetup(activity));
        }
    }

    public static void writeToSharedPrefs(Context context, Setup setup) {
        SharedPreferences.Editor editor = context.getSharedPreferences(DemoApplication.PREF_NAME, MODE_PRIVATE).edit();

        if (setup == null) {
            editor.remove(DemoApplication.KEY_API_KEY);
        } else {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.append(setup.getApikey()).append(";").append(setup.getName()).append(";").append(setup.getRegion());
            if (setup.getLiveApikey() != null) {
                strBuilder.append(";").append(setup.getLiveApikey());
            }
            editor.putString(DemoApplication.KEY_API_KEY, strBuilder.toString());
        }

        editor.apply();
    }

    public static Setup getCurrentSetup(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(DemoApplication.PREF_NAME, MODE_PRIVATE);
        String currentSetup = preferences.getString(DemoApplication.KEY_API_KEY, DemoApplication.NO_APIKEY);

        if (currentSetup.equals(DemoApplication.NO_APIKEY)) {
            currentSetup = DemoApplication.getDefaultSetup().getApikey() + ";" + DemoApplication.getDefaultSetup().getName()
                    + ";" + DemoApplication.getDefaultSetup().getRegion() + ";" + DemoApplication.getDefaultSetup().getLiveApikey();
        }

        String[] split = currentSetup.split(";");
        int size = split.length;

        Setup setup = new Setup();
        setup.setApikey(split[0]);
        setup.setName(split[1]);

        if (size >= 3) {
            setup.setRegion(split[2]);
        }

        if (size >= 4) {
            setup.setLiveApikey(split[3]);
        }

        return setup;
    }

    /*
     * Reading deep link intent
     */
    private static String[] readDeepLink(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            String tmp = data.toString();

            String[] deepLink = new String[4];

            // Adding default region - gets overridden if deeplink contains another region
            String region = DemoApplication.DEFAULT_REGION;
            deepLink[2] = region;

            UrlQuerySanitizer query = new UrlQuerySanitizer(tmp);
            for (UrlQuerySanitizer.ParameterValuePair pair : query.getParameterList()) {

                switch (pair.mParameter) {
                    case "apikey":
                        deepLink[0] = pair.mValue;
                        break;
                    case "name":
                        deepLink[1] = pair.mValue;
                        break;
                    case "region":
                        deepLink[2] = pair.mValue;
                        break;
                    case "liveApikey":
                        deepLink[3] = pair.mValue;
                        break;
                }
            }

            return deepLink;

        } else {
            return null;
        }
    }

    /*
     * Param apiKey includes both; apikey token and name -> remember to split in Application class
     */
    @SuppressLint("ApplySharedPref")
    private static void initSetup(Context context, Setup setup) {
        if (!DemoApplication.USES_DEFAULT_CONFIG) {
            writeToSharedPrefs(context, setup);
        }
        sendBroadcast(context, setup);
    }

    /*
     * Notify application class that we have found valid apikey
     */
    private static void sendBroadcast(Context context, Setup setup) {
        Intent intent = new Intent(BROADCAST_APIKEY_READY);
        intent.putExtra("setup", setup);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    // Try to download buildings to verify that key is valid
    public static void verifyApikey(String apikey, Callback callback) {
        HttpUrl url = HttpUrl.get("https://meta2.eu.steerpath.com/meta/v2/buildings");
        Request.Builder builder = new Request.Builder().get();
        builder.url(url);
        builder.header("Authorization", apikey);

        final Request request = builder.build();
        DEFAULT_OK_HTTP_CLIENT.newCall(request).enqueue(callback);
    }
}
