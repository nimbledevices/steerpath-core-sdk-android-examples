package com.steerpath.demo.utils;
import android.content.Context;
import android.graphics.Color;

import androidx.core.graphics.ColorUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import com.steerpath.demo.R;
import com.steerpath.sdk.maps.SteerpathAnnotationOptions;
import com.steerpath.sdk.maps.SteerpathLayerOptions;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.utils.internal.DrawableUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textFont;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textHaloBlur;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textHaloColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textHaloWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textSize;

public class FeatureCollectionFactory {

    private static final String MARKER_SOURCE = "marker";
    private static final String MARKER_ICON = "category_marker";

    private static Expression showAllInitially = Expression.has("id");
    private static Expression hideAllInitially = Expression.eq(Expression.get("id"), "invalid");

    public static void addMarker(SteerpathMap map, MetaFeature feature) {
        List<MetaFeature> features = new ArrayList<>();
        features.add(feature);
        addMarkers(map, features);
    }

    /**
     * Add markers to default source. Uses default marker icon
     */
    public static void addMarkers(SteerpathMap map, List<MetaFeature> features) {
        doAddMarkers(map, features, MARKER_SOURCE, MARKER_ICON);
    }

    /**
     * Add markers to specific source and use specific icon image. Icon image needs to be added to the map style before that.
     * If source and icon image are null, default ones will be used.
     */
    public static void addMarkers(Context context, SteerpathMap map, List<MetaFeature> features, String sourceId, String iconName, int iconDrawable) {

        if (iconName != null) {
            if (map.getImage(iconName) == null) {
                map.addImage(iconName, DrawableUtils.toBitmap(context.getResources(), iconDrawable));
            }
        } else {
            iconName = MARKER_ICON;
        }


        if (sourceId == null) sourceId = MARKER_SOURCE;

        doAddMarkers(map, features, sourceId, iconName);
    }


    private static void doAddMarkers(SteerpathMap map, List<MetaFeature> features, String sourceId, String iconImage) {
        if (map.hasSource(sourceId)) {
            Source source = map.getSource(sourceId);
            if (source instanceof GeoJsonSource) {
                GeoJsonSource geoJsonSource = (GeoJsonSource) source;
                geoJsonSource.setGeoJson(toFeatureCollection(features, iconImage));
            }
        } else {
            Source markerSource = new GeoJsonSource(sourceId, toFeatureCollection(features, iconImage));
            addLayer(map, sourceId, iconImage, showAllInitially);
            map.addSource(markerSource);
        }
    }

    private static void addLayer(SteerpathMap map, String sourceId, String iconImage, Expression filter) {
        SymbolLayer symbolLayer = new SymbolLayer(sourceId + "-layer", sourceId)
                .withProperties(
                        iconImage(iconImage),
                        iconAllowOverlap(true)
                );
        symbolLayer.setFilter(filter);
        map.addLayer(symbolLayer);
    }

    /**
     * Remove all markers from default source.
     */
    public static void removeMarkers(SteerpathMap map) {
        doRemoveMarkers(map, MARKER_SOURCE);
    }

    /**
     * Remove all markers from specific source.
     */
    public static void removeMarkers(SteerpathMap map, String sourceId) {
        doRemoveMarkers(map, sourceId);
    }

    private static void doRemoveMarkers(SteerpathMap map, String sourceId) {
        if (map.hasSource(sourceId)) {
            Source source = map.getSource(sourceId);
            if (source instanceof GeoJsonSource) {
                GeoJsonSource geoJsonSource = (GeoJsonSource) source;
                geoJsonSource.setGeoJson(toFeatureCollection(new ArrayList<>(), ""));
            }
        }
    }

    public static FeatureCollection toFeatureCollection(List<MetaFeature> metaFeatures) {
        Feature[] featureList = new Feature[metaFeatures.size()];
        for (int i=0; i<metaFeatures.size(); i++) {
            MetaFeature metaFeature = metaFeatures.get(i);
            featureList[i] = Feature.fromGeometry(Point.fromLngLat(metaFeature.getLongitude(), metaFeature.getLatitude()), featureProperties(metaFeature, null));
        }
        return FeatureCollection.fromFeatures(featureList);
    }

    private static FeatureCollection toFeatureCollection(List<MetaFeature> features, String iconImage) {
        Feature[] featureList = new Feature[features.size()];
        for (int i = 0; i<features.size(); i++) {
            featureList[i] = Feature.fromGeometry(Point.fromLngLat(features.get(i).getLongitude(), features.get(i).getLatitude()), featureProperties(features.get(i), iconImage));
        }

        return FeatureCollection.fromFeatures(featureList);
    }

    private static JsonObject featureProperties(MetaFeature feature, String iconImage) {
        JsonObject object = new JsonObject();
        if (iconImage != null) {
            object.add("iconImage", new JsonPrimitive(iconImage));
        }
        object.add("title", new JsonPrimitive(feature.getTitle()));
        object.add("css_class", new JsonPrimitive(""));
        object.add("localRef", new JsonPrimitive(feature.getLocalRef()));
        object.add(SteerpathLayerOptions.LAYER_INDEX, new JsonPrimitive(feature.getFloorIndex()));
        object.add(SteerpathLayerOptions.BUILDING_REF, new JsonPrimitive(feature.getBuildingRef()));
        object.add("id", new JsonPrimitive(feature.getId()));
        return object;
    }

    // Add new source and layer to the map to visualize geofence
    public static void addGeofence(Context context, SteerpathMap map, String sourceId, String layerId, JSONObject polygon) {
        map.addSource(new GeoJsonSource(sourceId, Polygon.fromJson(polygon.toString())));

        int color = context.getResources().getColor(R.color.geofence_area);
        int colorWithAlpha = ColorUtils.setAlphaComponent(color, 125);

        FillLayer fillLayer = new FillLayer(layerId, sourceId)
                .withProperties(
                        fillColor(colorWithAlpha)
                );
        map.addLayer(fillLayer);
    }

    public static SymbolLayer createSymbolLayer(SteerpathMap map, String sourceId, String layerId, String iconImage, Expression filter) {
        return new SymbolLayer(layerId, sourceId)
                .withFilter(filter)
                .withProperties(
                        iconImage(iconImage),
                        iconAllowOverlap(true),
                        textField("{text}"),
                        textColor(Color.parseColor("#9400D3")),
                        textSize(15f),
                        textAnchor(Property.TEXT_ANCHOR_TOP),
                        textAllowOverlap(true),
                        textHaloColor(Color.BLACK),
                        textHaloWidth(0.5f),
                        textHaloBlur(0.5f),
                        textFont(new String[] {"arial"})
                );
    }
}

