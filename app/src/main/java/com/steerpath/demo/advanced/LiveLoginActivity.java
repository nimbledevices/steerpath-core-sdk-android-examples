package com.steerpath.demo.advanced;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.steerpath.demo.DemoApplication;
import com.steerpath.demo.MainActivity;
import com.steerpath.demo.R;
import com.steerpath.demo.database.Setup;
import com.steerpath.demo.utils.ExpandAnimation;
import com.steerpath.demo.utils.SetupHelper;
import com.steerpath.demo.widgets.SimpleAdapter;
import com.steerpath.sdk.live.LiveMapOptions;
import com.steerpath.sdk.live.LiveOptions;
import com.steerpath.sdk.live.LiveServices;
import com.steerpath.sdk.live.LoginCallback;
import com.steerpath.sdk.utils.internal.Monitor;

import java.util.ArrayList;
import java.util.Arrays;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LiveLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button login;
    private Button edit;
    private Button update;

    private EditText userId;
    private EditText password;
    private EditText title;
    private EditText group;
    private EditText geofenceNeutral;
    private EditText geofenceAllowed;
    private EditText geofenceForbidden;

    private SwitchCompat switchCompat;

    private TextView titleText;

    private View layoutGroups;
    private View geofencesWrapper;
    private View layoutNeutralGeofences;
    private View layoutAllowedGeofences;
    private View layoutForbiddenGeofences;
    private View expandIcon;

    private LiveOptions liveOptions;

    private Setup setup;

    private SimpleAdapter groupsAdapter;
    private SimpleAdapter neutralGeofencesAdapter;
    private SimpleAdapter allowedGeofencesAdapter;
    private SimpleAdapter forbiddenGeofencesAdapter;

    private boolean loggedIn = false;
    private boolean loggedOut = false;
    private boolean inEditMode = false;
    private boolean isUp;
    private boolean showThisDevice;

    private float rotationAngle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_login);
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.steerpath_live);
        }

        setup = SetupHelper.getCurrentSetup(this);

        View noLiveApikey = findViewById(R.id.no_live_apikey);
        View liveLoginView = findViewById(R.id.scroll_view);
        login = findViewById(R.id.button_login);

        if (setup.getLiveApikey() == null) {
            liveLoginView.setVisibility(GONE);
            noLiveApikey.setVisibility(VISIBLE);
            login.setVisibility(GONE);

        } else {
            titleText = findViewById(R.id.title_text);

            View expandGeofences = findViewById(R.id.show_geofences);
            expandGeofences.setOnClickListener(this);
            geofencesWrapper = findViewById(R.id.layout_geofences);

            isUp = false;

            if (DemoApplication.getLiveOptions() != null) {
                liveOptions = DemoApplication.getLiveOptions();
            }

            // InputLayouts
            layoutGroups = findViewById(R.id.input_layout_group);
            layoutNeutralGeofences = findViewById(R.id.input_layout_neutral);
            layoutAllowedGeofences = findViewById(R.id.input_layout_allowed);
            layoutForbiddenGeofences = findViewById(R.id.input_layout_forbidden);

            // EditText fields
            userId = findViewById(R.id.setId);
            password = findViewById(R.id.setPassword);
            title = findViewById(R.id.setTitle);
            group = findViewById(R.id.setGroup);
            geofenceNeutral = findViewById(R.id.setNeutralGeofence);
            geofenceAllowed = findViewById(R.id.setAllowedGeofence);
            geofenceForbidden = findViewById(R.id.setForbiddenGeofence);

            expandIcon = findViewById(R.id.icon_expand);

            // Buttons
            ImageButton addGroup = findViewById(R.id.add_group);
            ImageButton addNeutralGeofence = findViewById(R.id.add_neutral_geofence);
            ImageButton addAllowedGeofence = findViewById(R.id.add_allowed_geofence);
            ImageButton addForbiddenGeofence = findViewById(R.id.add_forbidden_geofence);

            addGroup.setOnClickListener(this);
            addNeutralGeofence.setOnClickListener(this);
            addAllowedGeofence.setOnClickListener(this);
            addForbiddenGeofence.setOnClickListener(this);
            login.setOnClickListener(this);

            switchCompat = findViewById(R.id.switch_show_device);

            // TODO: if navigating to this activity while logged in and showThisDevice has previously
            // TODO: been set to true, it will be switched to false. FIX THIS BUG

            switchCompat.setChecked(DemoApplication.showThisDeviceLive);

            // This is just used for checking if user changes the showThisDevice configuration.
            showThisDevice = DemoApplication.showThisDeviceLive;

            switchCompat.setOnCheckedChangeListener((compoundButton, b) -> {
                DemoApplication.showThisDeviceLive = b;
            });

            initRecyclerViews();

            if (DemoApplication.liveEnabled) {
                initLoggedInView();
            }
        }
    }

    private void initLoggedInView() {
        View actionButtonsView = findViewById(R.id.action_buttons);
        actionButtonsView.setVisibility(VISIBLE);

        titleText.setText(R.string.logged_in);
        userId.setText(liveOptions.getUserId());
        password.setText(liveOptions.getPassword());
        title.setText(liveOptions.getTitle());

        edit = actionButtonsView.findViewById(R.id.button_edit);
        update = actionButtonsView.findViewById(R.id.button_update);
        Button logOut = actionButtonsView.findViewById(R.id.button_log_out);

        edit.setOnClickListener(this);
        update.setOnClickListener(this);
        logOut.setOnClickListener(this);

        userId.setEnabled(false);
        password.setEnabled(false);

        disableForm();

        login.setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_group:
                updateGeofenceList(group, groupsAdapter);
                break;

            case R.id.add_neutral_geofence:
                updateGeofenceList(geofenceNeutral, neutralGeofencesAdapter);
                break;

            case R.id.add_allowed_geofence:
                updateGeofenceList(geofenceAllowed, allowedGeofencesAdapter);
                break;

            case R.id.add_forbidden_geofence:
                updateGeofenceList(geofenceForbidden, forbiddenGeofencesAdapter);
                break;

            case R.id.button_login:
                if (isEmpty(userId) || isEmpty(password)) {

                    if (isEmpty(userId)) {
                        setError(userId, "User ID required!");
                    }

                    if (isEmpty(password)) {
                        setError(password, "Password required!");
                    }

                } else {
                    liveOptions = new LiveOptions()
                            .accessToken(setup.getLiveApikey())
                            .userId(userId.getText().toString())
                            .password(password.getText().toString())
                            .title(title.getText().toString());

                    if (groupsAdapter.getItemCount() != 0) {
                        liveOptions.userGroups(groupsAdapter.getItems());
                    }

                    if (neutralGeofencesAdapter.getItemCount() != 0) {
                        liveOptions.neutralGeofences(neutralGeofencesAdapter.getItems());
                    }

                    if (allowedGeofencesAdapter.getItemCount() != 0) {
                        liveOptions.allowedGeofences(allowedGeofencesAdapter.getItems());
                    }

                    if (forbiddenGeofencesAdapter.getItemCount() != 0) {
                        liveOptions.forbiddenGeofences(forbiddenGeofencesAdapter.getItems());
                    }

                    DemoApplication.setLiveOptions(liveOptions);

                    loggedIn = true;

                    doLogin(liveOptions,
                            new LiveMapOptions().showThisDevice(DemoApplication.showThisDeviceLive),
                            false);
                }

                break;

            case R.id.button_log_out:
                LiveServices.getLive().logout(new LoginCallback() {
                    @Override
                    public void onLogout() {
                        // onLogout() comes from non-ui thread. Just use some View's Handler to switch execution to the main thread.
                        login.post(() -> {
                            Toast.makeText(LiveLoginActivity.this, "Logged Out", Toast.LENGTH_LONG).show();
                            DemoApplication.liveEnabled = false;
                            DemoApplication.showThisDeviceLive = false;
                            loggedOut = true;
                            loggedIn = false;

                            // Easy way to reset default state
                            setContentView(R.layout.activity_live_login);
                            initView();
                        });
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(LiveLoginActivity.this, "Logout failed: " + error, Toast.LENGTH_LONG).show();
                    }
                });


                break;

            case R.id.button_edit:

                // Cancel edit mode
                if (inEditMode) {
                    initLoggedInView();
                } else {
                    enableForm();
                }

                break;

            case R.id.button_update:
                liveOptions.title(title.getText().toString());
                liveOptions.userGroups(groupsAdapter.getItems());
                liveOptions.neutralGeofences(neutralGeofencesAdapter.getItems());
                liveOptions.allowedGeofences(allowedGeofencesAdapter.getItems());
                liveOptions.forbiddenGeofences(forbiddenGeofencesAdapter.getItems());

                DemoApplication.setLiveOptions(liveOptions);

                doLogin(liveOptions,
                        new LiveMapOptions().showThisDevice(DemoApplication.showThisDeviceLive),
                        true);

                break;

            case R.id.show_geofences:
                if(isUp){
                    ExpandAnimation a = new ExpandAnimation(geofencesWrapper, 250, ExpandAnimation.COLLAPSE);
                    geofencesWrapper.startAnimation(a);
                } else {
                    ExpandAnimation a = new ExpandAnimation(geofencesWrapper, 250, ExpandAnimation.EXPAND);
                    geofencesWrapper.startAnimation(a);
                }

                animateRotation(expandIcon);
                isUp = !isUp;

                break;
        }
    }

    private void doLogin(LiveOptions transmitOptions, LiveMapOptions receiveOptions, boolean update) {
        if (transmitOptions != null) {
            LiveServices.getLive().login(transmitOptions, receiveOptions, new LoginCallback() {
                @Override
                public void onLogin() {
                    DemoApplication.liveEnabled = true;

                    // onLogin() comes from non-ui thread. Just use some View's Handler to switch execution to the main thread.
                    login.post(() -> {
                        initLoggedInView();

                        loggedIn = true;
                        loggedOut = false;

                        if (!update) {
                            Toast.makeText(LiveLoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LiveLoginActivity.this, "Update successful", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onError(String error) {
                    if (!update) {
                        Monitor.add(Monitor.TAG_LIVE, error);
                        if (error.contains("Wrong password for user")) {
                            setError(password, "Wrong password for user or username already be taken.");
                            Toast.makeText(LiveLoginActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
                        } else {
                            if (error.contains("Missing valid API")) {
                                showErrorDialog("Missing valid API key from request.");
                            } else if (error.contains("with write access")) {
                                showErrorDialog("You need an API key with write access to use this API.");
                            } else {
                                showErrorDialog(error);
                            }
                        }
                    }
                }
            });
        }
    }

    private void showErrorDialog(String errorText) {
        String msg = errorText + " " + getString(R.string.contact_steerpath);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login Error!");
        builder.setMessage(msg);
        builder.setPositiveButton("Close", null);
        builder.create().show();
    }

    private void updateGeofenceList(EditText editText, SimpleAdapter adapter) {
        if (!editText.getText().toString().isEmpty()) {
            adapter.addItem(editText.getText().toString());
            adapter.notifyDataSetChanged();
            editText.setText("");
        }
    }

    private void initRecyclerViews() {

        boolean loggedIn = DemoApplication.liveEnabled;

        // GROUPS
        RecyclerView groups = findViewById(R.id.recycler_view_groups);
        groups.setLayoutManager(new LinearLayoutManager(this));
        groups.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        groupsAdapter = new SimpleAdapter(this, new ArrayList<>());
        if (loggedIn) {
            if (liveOptions.getUserGroups() != null) {
                groupsAdapter = new SimpleAdapter(this, new ArrayList<>(Arrays.asList(liveOptions.getUserGroups())));
            }
        }

        groups.setAdapter(groupsAdapter);

        //NEUTRAL GEOFENCES
        RecyclerView neutral = findViewById(R.id.recycler_view_geofence_neutral);
        neutral.setLayoutManager(new LinearLayoutManager(this));
        neutral.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        neutralGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>());
        if (loggedIn) {
            if (liveOptions.getNeutralGeofences() != null) {
                neutralGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>(Arrays.asList(liveOptions.getNeutralGeofences())));
            }
        }

        neutral.setAdapter(neutralGeofencesAdapter);

        //ALLOWED GEOFENCES
        RecyclerView allowed = findViewById(R.id.recycler_view_geofence_allowed);
        allowed.setLayoutManager(new LinearLayoutManager(this));
        allowed.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        allowedGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>());
        if (loggedIn) {
            if (liveOptions.getAllowedGeofences() != null) {
                allowedGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>(Arrays.asList(liveOptions.getAllowedGeofences())));
            }
        }

        allowed.setAdapter(allowedGeofencesAdapter);

        //FORBIDDEN GEOFENCES
        RecyclerView forbidden = findViewById(R.id.recycler_view_geofence_forbidden);
        forbidden.setLayoutManager(new LinearLayoutManager(this));
        forbidden.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        forbiddenGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>());
        if (loggedIn) {
            if (liveOptions.getForbiddenGeofences() != null) {
                forbiddenGeofencesAdapter = new SimpleAdapter(this, new ArrayList<>(Arrays.asList(liveOptions.getForbiddenGeofences())));
            }
        }

        forbidden.setAdapter(forbiddenGeofencesAdapter);
    }

    @Override
    public void onBackPressed() {
        if (loggedIn) {
            Intent intent = new Intent();
            intent.setAction("LOGGED_IN");
            setResult(RESULT_OK, intent);
            finishActivity(MainActivity.LIVE_STATUS_CHANGED);
            super.onBackPressed();
        }

        if (loggedOut) {
            Intent intent = new Intent();
            intent.setAction("LOGGED_OUT");
            setResult(RESULT_OK, intent);
            finishActivity(MainActivity.LIVE_STATUS_CHANGED);
            super.onBackPressed();
        }

        if (showThisDevice != DemoApplication.showThisDeviceLive) {
            Intent intent = new Intent();
            intent.setAction("DEVICE_VISIBILITY");
            setResult(RESULT_OK, intent);
            finishActivity(MainActivity.LIVE_STATUS_CHANGED);
            super.onBackPressed();
        }

        super.onBackPressed();
    }

    private boolean isEmpty(EditText editText) {
        String input = editText.getText().toString().trim();
        return input.length() == 0;
    }

    private void setError(EditText editText, String errorString) {
        editText.setError(errorString);
    }

    private void enableForm() {

        inEditMode = true;

        title.setEnabled(true);

        update.setClickable(true);
        update.setAlpha(1f);

        edit.setText(getString(R.string.cancel));
        edit.setBackgroundColor(getResources().getColor(R.color.error_button));

        showRemoveButtons(true);

        switchCompat.setEnabled(true);

        layoutGroups.setVisibility(VISIBLE);
        layoutNeutralGeofences.setVisibility(VISIBLE);
        layoutAllowedGeofences.setVisibility(VISIBLE);
        layoutForbiddenGeofences.setVisibility(VISIBLE);
    }

    private void disableForm() {

        inEditMode = false;

        title.setEnabled(false);

        update.setClickable(false);
        update.setAlpha(.5f);

        edit.setText(getString(R.string.edit));
        edit.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        switchCompat.setEnabled(false);

        showRemoveButtons(false);

        // hide input layouts
        layoutGroups.setVisibility(GONE);
        layoutNeutralGeofences.setVisibility(GONE);
        layoutAllowedGeofences.setVisibility(GONE);
        layoutForbiddenGeofences.setVisibility(GONE);
    }

    private void showRemoveButtons(boolean show) {
        groupsAdapter.showRemoveButton(show);
        neutralGeofencesAdapter.showRemoveButton(show);
        allowedGeofencesAdapter.showRemoveButton(show);
        forbiddenGeofencesAdapter.showRemoveButton(show);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        }

        return super.dispatchTouchEvent(event);
    }

    private void animateRotation(View view) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(250);
        anim.start();

        rotationAngle += 180;
        rotationAngle = rotationAngle % 360;
    }
}
