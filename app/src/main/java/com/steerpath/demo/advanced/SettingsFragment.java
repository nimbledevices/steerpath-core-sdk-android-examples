package com.steerpath.demo.advanced;

import android.content.Context;
import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import com.steerpath.demo.R;
import com.steerpath.sdk.location.LocationServices;

import java.util.Objects;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);

        final SwitchPreferenceCompat accelerometer = findPreference("accelerometer");
        Objects.requireNonNull(accelerometer).setChecked(LocationServices.getGuideOptions().isAccelerometerEnabled());

        final SwitchPreferenceCompat compass = findPreference("compass");
        Objects.requireNonNull(compass).setChecked(LocationServices.getGuideOptions().isCompassEnabled());

        final SwitchPreferenceCompat gyroscope = findPreference("gyroscope");
        Objects.requireNonNull(gyroscope).setChecked(LocationServices.getGuideOptions().isGyroscopeEnabled());
    }
}
