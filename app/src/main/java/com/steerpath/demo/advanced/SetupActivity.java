package com.steerpath.demo.advanced;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.steerpath.demo.DemoApplication;
import com.steerpath.demo.MainActivity;
import com.steerpath.demo.R;
import com.steerpath.demo.database.DatabaseHelper;
import com.steerpath.demo.database.Setup;
import com.steerpath.demo.database.SetupDatabase;
import com.steerpath.demo.utils.SettingsHelper;
import com.steerpath.demo.utils.SetupHelper;
import com.steerpath.demo.widgets.ItemClickListener;
import com.steerpath.demo.widgets.SetupAdapter;
import com.steerpath.sdk.assettracking.AssetGateway;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.meta.MetaLoader;
import com.steerpath.sdk.meta.MetaQuery;

import java.io.IOException;
import java.util.ArrayList;

public class SetupActivity extends AppCompatActivity implements ItemClickListener {

    private Context context;

    private static Setup currentKey;

    private SetupAdapter adapter;

    private static ArrayList<MetaFeature> newBuildings;

    private AlertDialog dialog;

    private EditText editName;
    private EditText editApikey;
    private EditText editLiveApikey;
    private EditText editRegion;

    private View noApikey;
    private View loadingScreen;

    private static boolean clientChanged = false;
    private boolean isLoading = false;
    private boolean gatewayStopped = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        this.context = this;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadingScreen = findViewById(R.id.loading_screen);
        FloatingActionButton fabAddApikey = findViewById(R.id.fab_add_apikey);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_apikeys);
        noApikey = findViewById(R.id.view_contact_steerpath);

        fabAddApikey.setOnClickListener(view -> createDialog());

        new DatabaseHelper.SetupLoaderTask(this, setups -> {
            recyclerView.setLayoutManager(new LinearLayoutManager(SetupActivity.this));

            adapter = new SetupAdapter(SetupActivity.this, setups);
            adapter.setOnClickListener(SetupActivity.this);
            adapter.setActionModeListener(new SetupAdapter.ActionModeListener() {
                @Override
                public void actionMode(boolean isOn) {
                    if (isOn) fabAddApikey.hide();
                    else fabAddApikey.show();
                }

                /* This one is only for notifying adapter if currently used apikey is removed */
                @Override
                public void removedKeys(ArrayList<Setup> removedKeys) {
                    if (removedKeys.contains(currentKey)) {
                        if (setups.size() == 0) {
                            Intent intent = new Intent();
                            intent.putExtra("apikey", "");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        } else {
                            currentKey = setups.get(0);
                            adapter.selectedItem();
                            DemoApplication.configureClient(getApplicationContext(), currentKey);
                        }

                        if (currentKey != null) {
                            adapter.selectedItem();
                        }
                    }
                }
            });

            recyclerView.addItemDecoration(new DividerItemDecoration(SetupActivity.this, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(adapter);

            if (setups.isEmpty()) noApikey.setVisibility(View.VISIBLE);
            else noApikey.setVisibility(View.GONE);

            LocalBroadcastManager.getInstance(context).registerReceiver(sdkReadyBroadcastReceiver,
                    new IntentFilter(DemoApplication.BROADCAST_SDK_READY));

        }).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (isLoading) {
            return;
        }

        if (clientChanged) {
            if (gatewayStopped) {
                getIntent().putExtra("restart_gateway", true);
            }

            setResult(RESULT_OK, getIntent().putParcelableArrayListExtra("newBuildings", newBuildings));
            finishActivity(MainActivity.CLIENT_CHANGED);
        }

        LocalBroadcastManager.getInstance(context).unregisterReceiver(sdkReadyBroadcastReceiver);
        super.onBackPressed();
    }

    @Override
    public void onItemClick(View view, int position) {
        setLoadingScreen(true);
        isLoading = true;
        adapter.selectedItem();
        Setup selectedKey = adapter.getItem(position);
        currentKey = selectedKey;

        SetupHelper.writeToSharedPrefs(getApplicationContext(), selectedKey);
        DemoApplication.configureClient(getApplicationContext(), currentKey);
    }

    @Override
    public void onItemLongClick(View view, int position) {}

    public void loadBuildings() {
        MetaQuery.Builder query = new MetaQuery.Builder(this, MetaQuery.DataType.BUILDINGS);
        MetaLoader.load(query.build(), result -> {
            if (newBuildings != null) {
                newBuildings.clear();
            }
            newBuildings = result.getMetaFeatures();
            isLoading = false;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            setLoadingScreen(false);
        });
    }

    private void setTextChangedListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editName.getText().toString().equals("") && !editApikey.getText().toString().equals("")) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });
    }

    private void createDialog() {
        View dialogView = View.inflate(context, R.layout.dialog_add_apikey, null);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context)
                .setTitle("Add new API key")
                .setView(dialogView);

        editName = dialogView.findViewById(R.id.edit_text_name);
        editName.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        editName.setRawInputType(InputType.TYPE_CLASS_TEXT);
        setTextChangedListener(editName);

        editApikey = dialogView.findViewById(R.id.edit_text_apikey);
        editApikey.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editApikey.setRawInputType(InputType.TYPE_CLASS_TEXT);
        setTextChangedListener(editApikey);

        editLiveApikey = dialogView.findViewById(R.id.edit_text_live_apikey);
        editLiveApikey.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editApikey.setRawInputType(InputType.TYPE_CLASS_TEXT);

        editRegion = dialogView.findViewById(R.id.edit_text_region);
        editRegion.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editRegion.setRawInputType(InputType.TYPE_CLASS_TEXT);

        dialogBuilder.setPositiveButton("Confirm", (dialogInterface, i) -> {

            String key = editApikey.getText().toString();
            SetupHelper.verifyApikey(key, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("Failure", e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String res = response.body().string();
                    if (!response.isSuccessful()) {
                        SetupActivity.this.runOnUiThread(()
                                -> Toast.makeText(context, "Invalid API key!", Toast.LENGTH_LONG).show());
                    } else {
                        SetupActivity.this.runOnUiThread(() -> {
                            addNewApikey(key);
                        });
                    }
                }
            });
        });

        dialogBuilder.setNegativeButton("Cancel", null);

        dialog = dialogBuilder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnShowListener(dialogInterface ->
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false));
        dialog.show();
    }

    private void addNewApikey(String key) {
        String name = editName.getText().toString();
        String region = DemoApplication.DEFAULT_REGION;

        if (!editRegion.getText().toString().isEmpty()) {
            region = editRegion.getText().toString();
        }

        Setup setup = new Setup(key, name, region);

        String liveApikey = editLiveApikey.getText().toString();
        if (!liveApikey.isEmpty()) {
            setup.setLiveApikey(liveApikey);
        }

        SetupHelper.writeToSharedPrefs(getApplicationContext(), setup);
        currentKey = setup;

        // Add default setup
        SetupDatabase setupDatabase = SetupDatabase.getInstance(this);

        Setup defaultSetup = DemoApplication.getDefaultSetup();
        SetupDatabase.getInstance(this).getSetup(defaultSetup.getApikey(), setup12 -> {
            if (setup12 == null) {
                setupDatabase.insertSetup(defaultSetup);
                adapter.addItem(defaultSetup);
                adapter.notifyDataSetChanged();
            }
        });

        setupDatabase.getSetup(key, setup1 -> {
            if (setup1 != null) {
                setup1.setApikey(setup.getApikey());
                setup1.setRegion(setup.getRegion());
                if (setup.getName() != null) {
                    setup1.setName(setup.getName());
                }

                if (setup.getLiveApikey() != null) {
                    setup1.setLiveApikey(liveApikey);
                }

                setupDatabase.updateSetup(setup1);
                adapter.updateItem(setup1);
                adapter.notifyDataSetChanged();
            } else {
                setupDatabase.insertSetup(setup);
                adapter.addItem(setup);
                adapter.notifyDataSetChanged();
            }
        });

        if (noApikey.getVisibility() == View.VISIBLE) noApikey.setVisibility(View.GONE);

        DemoApplication.configureClient(getApplicationContext(), setup);
        if (SettingsHelper.useAssetGateway()) {
            AssetGateway.stopAssetGatewayService(context);
            gatewayStopped = true;
        }
    }

    private BroadcastReceiver sdkReadyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isLoading = false;
            SetupActivity.this.loadBuildings();
            clientChanged = true;
            if (DemoApplication.USES_DEFAULT_CONFIG) DemoApplication.USES_DEFAULT_CONFIG = false;
        }
    };

    public void setLoadingScreen(boolean enabled) {
        if (enabled) {
            loadingScreen.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeButtonEnabled(false);
            }

        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            loadingScreen.setVisibility(View.GONE);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }
    }
}
