package com.steerpath.demo.advanced;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import com.steerpath.demo.DemoApplication;
import com.steerpath.demo.MainActivity;
import com.steerpath.demo.R;
import com.steerpath.demo.utils.SettingsHelper;
import com.steerpath.demo.utils.SetupHelper;

import com.steerpath.sdk.assettracking.AssetGateway;
import com.steerpath.sdk.location.LocationServices;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.steerpath.demo.DemoApplication.BROADCAST_SDK_READY;

public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private View loadingScreen;

    private boolean showMonitor;
    private boolean useGPS;
    private boolean isGateway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadingScreen = findViewById(R.id.loading_screen);

        SettingsFragment settingsFragment = new SettingsFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, settingsFragment)
                .commit();


        showMonitor = SettingsHelper.useMonitor();
        useGPS = SettingsHelper.useHighAccuracyPositioning();
        isGateway = SettingsHelper.useAssetGateway();

        LocalBroadcastManager.getInstance(this).registerReceiver(sdkReadyBroadcastReceiver, new IntentFilter(BROADCAST_SDK_READY));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Have to register this listener onStart(). Otherwise listener gets triggered multiple times
        // when entering SettingsActivity first time.
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        if (showMonitor != SettingsHelper.useMonitor() || useGPS != SettingsHelper.useHighAccuracyPositioning()) {
            setResult(RESULT_OK, getIntent());
            finishActivity(MainActivity.SETTINGS_CHANGED);
        }

        super.onBackPressed();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("monitor") || key.equals("location_request")) {
            loadingScreen.setVisibility(VISIBLE);
            DemoApplication.configureClient(getApplicationContext(), SetupHelper.getCurrentSetup(this));
        }

        if (key.equals("asset_gateway")) {
            if (isGateway) {
                AssetGateway.stopAssetGatewayService(this);
            } else {
                AssetGateway.startAssetGatewayService(this);
            }

            isGateway = !isGateway;
        }

        if (key.equals("compass")) {
            LocationServices.getGuideOptions().compass(SettingsHelper.useCompass()).apply();
        }

        if (key.equals("gyroscope")) {
            LocationServices.getGuideOptions().gyroscope(SettingsHelper.useGyroscope()).apply();
        }

        if (key.equals("accelerometer")) {
            LocationServices.getGuideOptions().accelerometer(SettingsHelper.useAccelerometer()).apply();
        }
    }

    private BroadcastReceiver sdkReadyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadingScreen.setVisibility(GONE);
        }
    };

    @Override
    public void onDestroy() {
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }
}
