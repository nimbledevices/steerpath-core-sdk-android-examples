package com.steerpath.demo.widgets;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.steerpath.demo.DemoApplication;
import com.steerpath.demo.R;
import com.steerpath.demo.database.Setup;
import com.steerpath.demo.database.SetupDatabase;

import java.util.ArrayList;
import java.util.List;

public class SetupAdapter extends RecyclerView.Adapter<SetupAdapter.ViewHolder> {

    private Context context;

    private List<Setup> setups;

    private ItemClickListener clickListener;

    private boolean multiSelect = false;

    private ArrayList<Setup> selectedItems = new ArrayList<>();

    private ActionModeListener actionModeListener;

    public SetupAdapter(Context context, List<Setup> setups) {
        this.context = context;
        this.setups = setups;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_apikey, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Setup setup = setups.get(position);
        holder.update(setup);
    }

    @Override
    public int getItemCount() {
        return setups.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout container;
        private TextView name;
        private RadioButton radioButton;

        private ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container_apikey_row);
            name = itemView.findViewById(R.id.apikey);
            radioButton = itemView.findViewById(R.id.radio_btn_single_choice);
        }

        private void setSelected(Setup setup) {
            if (setup.getName().equals(getCurrentKey())) {
                radioButton.setChecked(true);
            } else radioButton.setChecked(false);
        }

        private void selecetRemovable(Setup setup) {
            if (multiSelect) {
                if (selectedItems.contains(setup)) {
                    selectedItems.remove(setup);
                    container.setBackgroundColor(Color.WHITE);
                } else {
                    selectedItems.add(setup);
                    container.setBackgroundColor(Color.LTGRAY);
                }
            }
        }

        private void update(final Setup setup) {
            name.setText(setup.getName());
            setSelected(setup);
            if (selectedItems.contains(setup)) container.setBackgroundColor(Color.LTGRAY);
            else container.setBackgroundColor(Color.WHITE);

            itemView.setOnClickListener(view -> {
                if (multiSelect) selecetRemovable(setup);
                else {
                    setSelected(setup);
                    clickListener.onItemClick(itemView, getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(view -> {
                clickListener.onItemLongClick(view, getAdapterPosition());
                ((AppCompatActivity)view.getContext()).startSupportActionMode(actionModeCallBacks);
                selecetRemovable(setup);
                return true;
            });
        }
    }

    public Setup getItem(int position) {
        return setups.get(position);
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }

    public void addItem(Setup setup) {
        setups.add(setup);
        notifyItemInserted(setups.size());
    }

    public void updateItem(Setup setup) {
        for (int i = 0; i < setups.size(); i++) {
            if (setup.getApikey().equals(setups.get(i).getApikey())) {
                setups.set(i, setup);
                notifyItemChanged(i);
            }
        }
    }

    private String getCurrentKey() {
        SharedPreferences prefs = context.getApplicationContext().getSharedPreferences(DemoApplication.PREF_NAME, Context.MODE_PRIVATE);
        String prefsSetup= prefs.getString(DemoApplication.KEY_API_KEY, DemoApplication.NO_APIKEY);
        if (!prefsSetup.equals(DemoApplication.NO_APIKEY)) {
            String[] parsedApikey = prefsSetup.split(";");
            return parsedApikey[1];
        } else {
            return setups.get(0).getName();
        }
    }

    public void setOnClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    private ActionMode.Callback actionModeCallBacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_mode_menu, menu);
            mode.setTitle("Delete selected key(s)");
            multiSelect = true;
            actionModeListener.actionMode(true);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            for (Setup setup : selectedItems) {
                setups.remove(setup);
                SetupDatabase.getInstance(context).deleteSetup(setup);
            }
            actionModeListener.removedKeys(selectedItems);
            mode.finish();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            multiSelect = false;
            selectedItems.clear();
            actionModeListener.actionMode(false);
            notifyDataSetChanged();
        }
    };

    public interface ActionModeListener {
        void actionMode(boolean isOn);
        void removedKeys(ArrayList<Setup> removedSetups);
    }

    public void setActionModeListener(ActionModeListener listener) {
        this.actionModeListener = listener;
    }
}
