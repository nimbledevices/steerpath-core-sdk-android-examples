package com.steerpath.demo.widgets;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.steerpath.demo.R;
import com.steerpath.sdk.meta.MetaFeature;

import java.util.List;

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.ViewHolder> {

    private Context context;
    private List<MetaFeature> features;
    private OnItemClickListener onItemClickListener;

    public FeatureAdapter(Context context, List<MetaFeature> features) {
        this.context = context;
        this.features = features;
    }

    @NonNull
    @Override
    public FeatureAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_feature, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeatureAdapter.ViewHolder holder, int position) {
        MetaFeature metaFeature = features.get(position);

        if (metaFeature.getTitle().isEmpty()) {
            holder.title.setText(R.string.no_title);
        } else {
            holder.title.setText(metaFeature.getTitle());
        }

        holder.floor.setText(context.getString(R.string.floor_index, metaFeature.getFloorIndex()));
        String tags = TextUtils.join(", ", metaFeature.getTags());
        holder.tags.setText(context.getString(R.string.tags_amount, tags));
        holder.id.setText(context.getString(R.string.id, metaFeature.getId()));
    }

    @Override
    public int getItemCount() {
        return features.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView floor;
        TextView tags;
        TextView id;

        ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.feature_title);
            floor = itemView.findViewById(R.id.feature_floor);
            tags = itemView.findViewById(R.id.feature_tags);
            id = itemView.findViewById(R.id.feature_id);

            itemView.setOnClickListener(view -> {
                onItemClickListener.onItemClick(itemView, getAdapterPosition());
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
