package com.steerpath.demo.widgets;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.steerpath.demo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.GONE;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.ViewHolder> {

    private Context context;
    private List<String> items;
    private boolean showRemoveButton = true;

    public SimpleAdapter(Context context, List<String> groups) {
        this.context = context;
        this.items = groups;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_simple_item, null);
        return new SimpleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(items.get(position));

        if (showRemoveButton) {
            holder.removeBtn.setVisibility(View.VISIBLE);
        } else {
            holder.removeBtn.setVisibility(GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageButton removeBtn;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            removeBtn = itemView.findViewById(R.id.remove_group);

            removeBtn.setOnClickListener(view -> removeItem(getAdapterPosition()));
        }
    }

    public void addItem(String item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    private void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public String[] getItems() {
        String[] array = new String[items.size()];
        items.toArray(array);
        return array;
    }

    public void showRemoveButton(boolean showRemoveButton) {
        this.showRemoveButton = showRemoveButton;
        notifyDataSetChanged();
    }
}
