package com.steerpath.demo.database;

import android.app.Activity;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.List;

public class DatabaseHelper {

    public DatabaseHelper(){}

    public static class SetupLoaderTask extends AsyncTask<Void, Void, List<Setup>> {
        private WeakReference<Activity> weakReference;
        private SetupLoaderListener setupLoaderListener;

        public SetupLoaderTask(Activity activity, SetupLoaderListener listener) {
            this.weakReference = new WeakReference<>(activity);
            this.setupLoaderListener = listener;
        }

        @Override
        protected List<Setup> doInBackground(Void... voids) {
            return SetupDatabase.getInstance(weakReference.get().getApplicationContext()).getSetups();
        }

        @Override
        protected void onPostExecute(List<Setup> setups) {
            setupLoaderListener.onLoaded(setups);
        }
    }

    public interface SetupLoaderListener {
        void onLoaded(List<Setup> setups);
    }
}
