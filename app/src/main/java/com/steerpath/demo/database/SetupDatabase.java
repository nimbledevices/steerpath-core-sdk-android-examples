package com.steerpath.demo.database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.List;

@Database(entities = {Setup.class}, version = 2, exportSchema = false)
public abstract class SetupDatabase extends RoomDatabase {

    private static final String DB_NAME = "steerpathdemo.DB";

    private static SetupDatabase INSTANCE;

    public abstract SetupDao setupDao();


    public static SetupDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, SetupDatabase.class, DB_NAME)
                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        return INSTANCE;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //Create new table
            database.execSQL("CREATE TABLE setup (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    "apikey TEXT NOT NULL, name TEXT NOT NULL, region TEXT NOT NULL, liveApikey TEXT)");

            // Copy data from old table to new one
            database.execSQL("INSERT INTO setup (id, apikey, name, region, liveApikey) "
                    + "SELECT _id, apikey, name, region, liveApikey "
                    + "FROM apikeys");

            // Remove old table
            database.execSQL("DROP TABLE apikeys");

            Log.d("Database", "migrated from 1 to 2");
        }
    };

    @SuppressLint("StaticFieldLeak")
    public void insertSetup(final Setup setup) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                setupDao().insertSetup(setup);
                return null;
            }
            @Override
            protected void onPostExecute(Void ready) {
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void updateSetup(final Setup setup) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                setupDao().updateSetup(setup);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void deleteSetup(final Setup setup) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                setupDao().deleteSetup(setup);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void getSetup(String apikey, SetupLoaderListener listener) {
        new AsyncTask<Void, Void, Setup>() {

            @Override
            protected Setup doInBackground(Void... voids) {
                return setupDao().getSetup(apikey);
            }

            @Override
            protected void onPostExecute(Setup setup) {
                listener.onLoaded(setup);
            }
        }.execute();
    }

    public List<Setup> getSetups() {
        return setupDao().fetchAllSetups();
    }

    public interface SetupLoaderListener {
        void onLoaded(Setup setup);
    }
}
