package com.steerpath.demo.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "setup")
public class Setup implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "apikey")
    private String apikey;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "region")
    private String region;

    @ColumnInfo(name = "liveApikey")
    private String liveApikey;

    public Setup(){}

    @Ignore
    public Setup(@NonNull String apikey, @NonNull String name, @NonNull String region) {
        this.apikey = apikey;
        this.name = name;
        this.region = region;
    }

    @Ignore
    public Setup(@NonNull String apikey, @NonNull String name, @NonNull String region, String liveApikey) {
        this.apikey = apikey;
        this.name = name;
        this.region = region;
        this.liveApikey = liveApikey;
    }

    void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @NonNull public String getApikey() {
        return apikey;
    }

    public void setApikey(@NonNull String apikey) {
        this.apikey = apikey;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getRegion() {
        return region;
    }

    public void setRegion(@NonNull String region) {
        this.region = region;
    }

    public String getLiveApikey() {
        return liveApikey;
    }

    public void setLiveApikey(String liveApikey) {
        this.liveApikey = liveApikey;
    }
}
