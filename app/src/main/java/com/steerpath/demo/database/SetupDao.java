package com.steerpath.demo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SetupDao {

    @Insert
    Long insertSetup(Setup setup);

    @Query("SELECT * FROM setup")
    List<Setup> fetchAllSetups();

    @Update
    void updateSetup(Setup setup);

    @Delete
    void deleteSetup(Setup setup);

    @Query("SELECT * FROM setup WHERE apikey = :apikey LIMIT 1")
    Setup getSetup(String apikey);
}
